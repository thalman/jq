#!/bin/sh

die () {
    echo "$1" >&2
    exit 1
}

# link jq binary. This make the tests to use
# installed binary instead of compiled one
cd source || die "missing source directory"
rm -f jq tests/*.log 2>/dev/null
ln -s /usr/bin/jq || die "failed to link jq binary"

# run the tests
# List of tests is taken from Makefile
TESTS="tests/optionaltest tests/mantest tests/jqtest tests/onigtest tests/shtest tests/utf8test tests/base64test"

for t in $TESTS; do
    echo -n "Test $t ... "
    ./${t} >"${t}.log" 2>&1
    RET=$?
    if [ $RET = 0 ]; then
        echo "ok"
    else
        echo "failed"
        cat "${t}.log"
        die "Test ${t} failed"
    fi
done
exit 0
